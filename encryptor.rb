class Encryptor
  def cipher(rotation)
    characters = (' '..'z').to_a
    rotated_characters = characters.rotate(rotation)
    Hash[characters.zip(rotated_characters)]
  end

  def encrypt_letter(letter,rotation)
    # rotation = 13
    cipher_for_rotation = cipher(rotation)
    cipher_for_rotation[letter]
  end

  def encrypt(string, rotation)
    letters = string.split("")

    results = []
    letters.each do |letter|
      encrypted_letter = encrypt_letter(letter, rotation)
      results.push(encrypted_letter)
    end

    results.join
  end

  def decrypt(string, rotation)
    letters = string.split("")

    results = []
    letters.each do |letter|
      encrypted_letter = encrypt_letter(letter, rotation)
      results.push(encrypted_letter)
    end

    results.join
  end

  def encrypt_file(filename, rotation)

    # Create the file handle to the input file
    # Read the text of the input file
    # Encrypt the text
    # Create a name for the output file
    # Create an output file handle
    # Write out the text
    # Close the file
    input = File.open(filename, "r")
    text = input.read
    encrypted = encrypt(text, rotation)
    output = File.open(filename+".encrypted", "w")
    out = output.write(encrypted)
    output.close

  end

  def decrypt_file(filename, rotation)
    input = File.open(filename, "r")
    text = input.read
    decrypted = decrypt(text, rotation)
    output = File.open(filename+".decrypted", "w")
    out = output.write(decrypted)
    output.close
  end

end
